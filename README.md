# Manajemen Basis Data
1. Langkah Pertama, gunakan kode yang ada pada file docker-compose.yml
![langkah 1](https://gitlab.com/evandika998/manajemen-basis-data/-/raw/main/1.basis-data.jpg?ref_type=heads)

```postgres
version: '3.5'

  services:
    postgres:
      container_name: postgres_container_Evandika_Nazwansyah
      image: postgres:16.0
      environment:
        POSTGRES_USER: ${POSTGRES_USER}
        POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
        PGDATA: /data/postgres
      volumes:
        - postgres:/data/postgres
      ports:
        - "21047:5432"
      networks:
        - postgres
      restart: unless-stopped
    
    pgadmin:
      container_name: pgadmin_container_Evandika_Nazwansyah
      image: dpage/pgadmin4
      environment:
        PGADMIN_DEFAULT_EMAIL: ${PGADMIN_DEFAULT_EMAIL:-pgadmin4@pgadmin.org}
        PGADMIN_DEFAULT_PASSWORD: ${PGADMIN_DEFAULT_PASSWORD:-admin}
        PGADMIN_CONFIG_SERVER_MODE: 'False'
      volumes:
        - pgadmin:/var/lib/pgadmin

      ports:
        - "${PGADMIN_PORT:-22047}:80"
      networks:
        - postgres
      restart: unless-stopped

  networks:
    postgres:
      driver: bridge

  volumes:
      postgres:
      pgadmin:
```
2. Buatlah agar POSTGRES_USER dan POSTGRES_PASSWORD privat, dengan membuat file baru .env
![langkah 2](https://gitlab.com/evandika998/manajemen-basis-data/-/raw/main/2.basis-data.jpg?ref_type=heads)

```environment
  POSTGRES_USER=postgres
  POSTGRES_PASSWORD=ifunggul
```
3. Gunakan CMD dan gunakan docker compose up -d
4. Masuk ke dalam aplikasi docker dan cek container
![langkah 3](https://gitlab.com/evandika998/manajemen-basis-data/-/raw/main/3.basis-data.jpg?ref_type=heads)
5. Masuk ke link yang ada pada pgadmin, untuk general > name, gunakan "localhost", lalu pada conection > password, gunakan "ifumggul"
![langkah 3](https://gitlab.com/evandika998/manajemen-basis-data/-/raw/main/4.basis-data.jpg?ref_type=heads)

